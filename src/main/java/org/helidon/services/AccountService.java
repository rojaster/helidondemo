/*
 * Copyright (c) 2018.
 * By Alekum. All Rights Reserved.
 */

package org.helidon.services;

import io.helidon.common.http.Http;
import io.helidon.common.http.MediaType;
import io.helidon.webserver.Routing;
import io.helidon.webserver.ServerRequest;
import io.helidon.webserver.ServerResponse;
import io.helidon.webserver.Service;
import org.helidon.dao.IEntityManager;
import org.helidon.messages.ContextMessages;
import org.helidon.models.accounts.Account;
import org.helidon.models.accounts.IAccount;
import org.helidon.models.transactions.Transaction;
import org.helidon.models.users.IUser;
import org.helidon.storages.inmemory.AccountsStorage;
import org.helidon.storages.inmemory.TransactionsStorage;
import org.helidon.storages.inmemory.UsersStorage;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import java.util.Collection;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AccountService extends AbstractService implements Service, IServiceHandlers
{
    private static final Logger logger = Logger.getLogger(AccountService.class.getName());

    public AccountService(IEntityManager<UUID,
                                        UsersStorage<UUID, IUser>,
                                        AccountsStorage<UUID, IAccount>,
                                        TransactionsStorage<UUID, Transaction>> _entityManager)
    {
        this.entityManager = _entityManager;

    }

    @Override
    public synchronized void update(Routing.Rules rules)
    {
        logger.info("AccountService is Registered");

        rules.get("/accounts", this::listHandler)
                .get("/accounts/{accountid}", this::getHandler)
                .post("/accounts", this::createHandler)
                .delete("/accounts/{accountid}", this::deleteHandler);
    }

    public synchronized void listHandler(ServerRequest req, ServerResponse res)
    {
        Collection<IAccount> accounts = entityManager.accounts().values();
        JsonArrayBuilder builder = Json.createArrayBuilder();

        accounts.stream().map(AccountService::makeAccountJsonObject).forEach(builder::add);

        JsonObject result = Json.createObjectBuilder().add("accounts",  builder.build()).build();

        res.headers().contentType(MediaType.APPLICATION_JSON);
        res.status(Http.Status.OK_200).send(result);
    }

    public synchronized void getHandler(ServerRequest req, ServerResponse res)
    {
        IAccount account = entityManager.accounts().get(UUID.fromString(req.path().param("accountid")));

        JsonObject result;
        Http.Status status;

        if(account != null)
        {
            result = AccountService.makeAccountJsonObject(account);
            status = Http.Status.OK_200;
        }
        else
        {
            status = Http.Status.CONFLICT_409;
            result = Json.createObjectBuilder().add("message",
                                                    status.toString()
                                                            + " : " + ContextMessages.ACCOUNT_CANT_BE_FOUND)
                                                .build();
        }
        res.headers().contentType(MediaType.APPLICATION_JSON);
        res.status(status).send(result);
    }

    public synchronized void createHandler(ServerRequest req, ServerResponse res)
    {
        req.content().as(JsonObject.class).thenAccept(payload -> {

            JsonObject result;
            Http.Status status;

            try
            {
                IUser user = entityManager.users().get(UUID.fromString(payload.getString("userid")));

                if(user != null)
                {
                    IAccount newAccount = new Account(user.getUserID());
                    entityManager.accounts().put(newAccount.getAccountID(), newAccount);
                    user.accountsIDs().add(newAccount.getAccountID());

                    result = AccountService.makeAccountJsonObject(newAccount);
                    status = Http.Status.CREATED_201;
                }
                else
                {
                    status = Http.Status.NOT_FOUND_404;
                    result = Json.createObjectBuilder().add("message", status.toString()).build();
                }

                res.headers().contentType(MediaType.APPLICATION_JSON);
                res.status(status).send(result);
            }
            catch(Exception e)
            {
                req.next(e);
            }
        });
    }

    public synchronized void updateHandler(ServerRequest req, ServerResponse res)
    {
        res.status(Http.Status.FORBIDDEN_403).send();
    }

    public synchronized void deleteHandler(ServerRequest req, ServerResponse res)
    {
        IAccount account = entityManager.accounts().get(UUID.fromString(req.path().param("accountid")));

        JsonObject result;
        Http.Status status;

        if(account != null)
        {
            entityManager.users().get(account.getAccountHolderID()).accountsIDs().remove(account.getAccountID());

            account.transactions()
                    .parallelStream()
                    .forEach(entityManager.transactions()::remove);


            result = AccountService.makeAccountJsonObject(account);

            logger.log(Level.INFO, String.format("DELETED: accountID %s",result.toString()));

            entityManager.accounts().remove(account.getAccountID());

            status = Http.Status.ACCEPTED_202;
        }
        else
        {
            status = Http.Status.NOT_FOUND_404;
            result = Json.createObjectBuilder()
                            .add("message", ContextMessages.ACCOUNT_CANT_BE_FOUND)
                            .build();
        }

        res.headers().contentType(MediaType.APPLICATION_JSON);
        res.status(status).send(result);
    }

    private static JsonObject makeAccountJsonObject(IAccount account)
    {
        JsonArrayBuilder builder = Json.createArrayBuilder();
        account.transactions().parallelStream().map(UUID::toString).forEach(builder::add);

        return Json.createObjectBuilder()
                    .add("userid", account.getAccountHolderID().toString())
                    .add("accountid", account.getAccountID().toString())
                    .add("balance", account.getAccountBalance().toString())
                    .add("transactions", builder.build())
                    .build();
    }
}
