/*
 * Copyright (c) 2018.
 * By Alekum. All Rights Reserved.
 */

package org.helidon.services;

import io.helidon.common.http.Http;
import io.helidon.common.http.MediaType;
import io.helidon.webserver.Routing;
import io.helidon.webserver.ServerRequest;
import io.helidon.webserver.ServerResponse;
import io.helidon.webserver.Service;
import org.helidon.dao.IEntityManager;
import org.helidon.messages.ContextMessages;
import org.helidon.models.accounts.IAccount;
import org.helidon.models.transactions.Transaction;
import org.helidon.models.transactions.TransactionStatus;
import org.helidon.models.users.IUser;
import org.helidon.storages.inmemory.AccountsStorage;
import org.helidon.storages.inmemory.TransactionsStorage;
import org.helidon.storages.inmemory.UsersStorage;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.UUID;
import java.util.logging.Logger;

public class TransactionService extends AbstractService implements Service, IServiceHandlers
{
    private static final Logger logger = Logger.getLogger(TransactionService.class.getName());

    public TransactionService(IEntityManager<UUID,
                                            UsersStorage<UUID, IUser>,
                                            AccountsStorage<UUID, IAccount>,
                                            TransactionsStorage<UUID, Transaction>> _entityManager)
    {
        this.entityManager = _entityManager;
    }

    @Override
    public synchronized void update(Routing.Rules rules)
    {
        logger.info("TransService is Registered");

        rules.get("/transactions", this::listHandler)
                .get("/transactions/{transactionid}", this::getHandler)
                .post("/transactions", this::createHandler)
                .put("/transactions/{transactionid}", this::updateHandler)
                .delete("/transactions/{transactionid}", this::deleteHandler);
    }

    public synchronized void listHandler(ServerRequest req, ServerResponse res)
    {
        Collection<Transaction> transactions = entityManager.transactions().values();
        JsonArrayBuilder builder =Json.createArrayBuilder();

        transactions.stream().map(TransactionService::makeTransactionJsonObject).forEach(builder::add);

        JsonObject result = Json.createObjectBuilder().add("transactions", builder.build()).build();

        res.headers().contentType(MediaType.APPLICATION_JSON);
        res.status(Http.Status.OK_200).send(result);
    }

    public synchronized void getHandler(ServerRequest req, ServerResponse res)
    {
        Transaction transaction = entityManager.transactions().get(UUID.fromString(req.path().param("transactionid")));

        JsonObject result;
        Http.Status status;

        if(transaction != null)
        {
            result = TransactionService.makeTransactionJsonObject(transaction);
            status = Http.Status.OK_200;
        }
        else
        {
            status = Http.Status.CONFLICT_409;
            result = Json.createObjectBuilder().add("message",
                                                    status.toString()
                                                            + " : " + ContextMessages.TRANSACTION_CANT_BE_FOUND)
                    .build();
        }

        res.headers().contentType(MediaType.APPLICATION_JSON);
        res.status(status).send(result);
    }


    public synchronized void createHandler(ServerRequest req, ServerResponse res)
    {
        req.content().as(JsonObject.class).thenAccept(payload -> {

            JsonObject result;
            Http.Status status;

            try
            {

                logger.info("CREATE TRANSACTION : " + payload.toString());

                IAccount accountSender = entityManager.accounts().get(UUID.fromString(payload.getString("from")));
                IAccount accountRecipient = entityManager.accounts().get(UUID.fromString(payload.getString("to")));

                if(accountSender != null && accountRecipient != null)
                {
                    BigDecimal amount = new BigDecimal(payload.getString("amount"));

                    Transaction t = new Transaction(accountSender.getAccountID(),
                                                    accountRecipient.getAccountID(),
                                                    amount,
                                                    TransactionStatus.PROCESSING);

                    entityManager.transactions().put(t.TransactionID, t);
                    accountSender.transactions().add(t.TransactionID);

                    result = TransactionService.makeTransactionJsonObject(t);
                    status = Http.Status.CREATED_201;
                }
                else
                {
                    status = Http.Status.NOT_FOUND_404;
                    result = Json.createObjectBuilder().add("message",
                                                            status.toString()
                                                                    + " : " + ContextMessages.ACCOUNT_CANT_BE_FOUND)
                            .build();
                }
                res.headers().contentType(MediaType.APPLICATION_JSON);
                res.status(status).send(result);
            }
            catch(Exception e)
            {
                req.next(e);
            }
        });

    }

    public synchronized void updateHandler(ServerRequest req, ServerResponse res)
    {
        Transaction transaction = entityManager.transactions().get(
                UUID.fromString(req.path().param("transactionid")));

        JsonObject result;
        Http.Status status;

        if(transaction != null && transaction.Status == TransactionStatus.PROCESSING)
        {
            IAccount from = entityManager.accounts().get(transaction.FromAccountID);
            IAccount to = entityManager.accounts().get(transaction.ToAccountID);

            boolean flag = TransactionService.proceedTransaction(from, to, transaction);

            status = flag ? Http.Status.OK_200 : Http.Status.I_AM_A_TEAPOT;

            result = TransactionService.makeTransactionJsonObject(transaction);
        }
        else
        {
            status = Http.Status.NOT_FOUND_404;
            result = Json.createObjectBuilder().add("message",
                                                    status.toString()
                                                            + " : " + ContextMessages.TRANSACTION_CANT_BE_FOUND).build();
        }

        res.headers().contentType(MediaType.APPLICATION_JSON);
        res.status(status).send(result);
    }


    public synchronized void deleteHandler(ServerRequest req, ServerResponse res)
    {
        Transaction transaction = entityManager.transactions().get(UUID.fromString(req.path().param("transactionid")));

        JsonObject result;
        Http.Status status;

        if(transaction != null)
        {
            entityManager.accounts().get(transaction.FromAccountID).transactions().remove(transaction.TransactionID);

            result = TransactionService.makeTransactionJsonObject(transaction);

            logger.info(String.format("TRANSACTION DELETED: %s", result.toString()));

            entityManager.transactions().remove(transaction.TransactionID);

            status = Http.Status.ACCEPTED_202;
        }
        else
        {
            status = Http.Status.NOT_FOUND_404;
            result = Json.createObjectBuilder().add("message", status.toString()).build();
        }

        res.headers().contentType(MediaType.APPLICATION_JSON);
        res.status(status).send(result);
    }

    private static JsonObject makeTransactionJsonObject(Transaction transaction)
    {
        return Json.createObjectBuilder()
                    .add("transactionid", transaction.TransactionID.toString())
                    .add("from"         , transaction.FromAccountID.toString())
                    .add("to"           , transaction.ToAccountID.toString())
                    .add("amount"       , transaction.Amount.toString())
                    .add("status"       , transaction.Status.toString())
                    .build();
    }

    private static boolean proceedTransaction(IAccount from, IAccount to, Transaction transaction)
    {
        if(transaction.Amount.signum() == 1)
        {
            // @Information(alekum):
            // It's supposed we always deposit money for self
            if(from.getAccountID().equals(to.getAccountID()))
            {
                from.setAccountBalance(from.getAccountBalance().add(transaction.Amount));
                transaction.Status = TransactionStatus.COMPLETE;
                return true;
            }
            else
            {
                if(from.getAccountBalance().compareTo(transaction.Amount) >= 0)
                {
                    from.setAccountBalance(from.getAccountBalance().subtract(transaction.Amount));
                    to.setAccountBalance(to.getAccountBalance().add(transaction.Amount));
                    transaction.Status = TransactionStatus.COMPLETE;
                    return true;
                }
            }
        }

        transaction.Status = TransactionStatus.DECLINED;
        return false;
    }
}
