/*
 * Copyright (c) 2018.
 * By Alekum. All Rights Reserved.
 */

package org.helidon.storages.inmemory;

import org.helidon.models.transactions.Transaction;

import java.util.UUID;
import java.util.HashMap;

public class TransactionsStorage<K extends UUID, V extends Transaction> extends HashMap<K, V>
{
}
