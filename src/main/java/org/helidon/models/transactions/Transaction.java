/*
 * Copyright (c) 2018.
 * By Alekum. All Rights Reserved.
 */

package org.helidon.models.transactions;

import java.math.BigDecimal;
import java.util.UUID;

/*
 * Definitely, this is not a java style to use structures like this, w/o interfaces.
 * -- alekum, 10/15/2018
 */
public class Transaction
{
    public final UUID TransactionID;
    public final UUID FromAccountID;
    public final UUID ToAccountID;
    public final BigDecimal Amount;
    public TransactionStatus Status;

    public Transaction(UUID _from, UUID _to,
                       BigDecimal _amount,
                       TransactionStatus _status)
    {
        this.FromAccountID = _from;
        this.ToAccountID   = _to;
        this.Amount        = _amount;
        this.Status        = _status;
        this.TransactionID = UUID.randomUUID();
    }


    @Override
    public boolean equals(Object obj)
    {
        if(obj instanceof Transaction)
        {
            Transaction rhs = (Transaction) obj;
            return this.TransactionID.equals(rhs.TransactionID);
        }
        return false;
    }

    @Override
    public int hashCode()
    {
        return this.TransactionID.hashCode() & super.hashCode();
    }
}
