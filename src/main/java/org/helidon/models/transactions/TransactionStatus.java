/*
 * Copyright (c) 2018.
 * By Alekum. All Rights Reserved.
 */

package org.helidon.models.transactions;

public enum TransactionStatus
{
    COMPLETE,
    DECLINED,
    PROCESSING
}
