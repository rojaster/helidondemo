/*
 * Copyright (c) 2018.
 * By Alekum. All Rights Reserved.
 */

package org.helidon.messages;


/*
* @Incomplete(alekum):
* This is dummy class for supporting different messages.
* Properties, normally, have to be read from a configuration file for a specific locale.
 */
public class ContextMessages
{
    protected ContextMessages(){}

    public static final String BAD_REQUEST_MESSAGE_RESPONSE = "UNABLE TO PARSE REQUEST";
    public static final String MAIN_PAGE_GREETING = "HELLO SAILOR!";

    public static final String USER_CANT_BE_FOUND = "SORRY SAILOR! REQUESTED USER CAN'T BE FOUND";
    public static final String USER_CREATION = "USER CREATION IS KICKED OFF";

    public static final String ACCOUNT_CANT_BE_FOUND = "SORRY SAILOR! REQUESTED ACCOUNT IS DISAPPEARED!";

    public static final String TRANSACTION_CANT_BE_FOUND = "SORRY COWBOY! REQUESTED TRANSACTIONS FLEW AWAY";
}
