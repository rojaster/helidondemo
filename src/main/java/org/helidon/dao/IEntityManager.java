/*
 * Copyright (c) 2018.
 * By Alekum. All Rights Reserved.
 */

package org.helidon.dao;

import org.helidon.models.accounts.IAccount;
import org.helidon.models.transactions.Transaction;
import org.helidon.models.users.IUser;

import java.util.Map;
import java.util.UUID;

public interface IEntityManager<K extends UUID,
                        U extends Map<K, IUser>,
                        A extends Map<K, IAccount>,
                        T extends Map<K, Transaction>>
{
    U users();
    A accounts();
    T transactions();
}
