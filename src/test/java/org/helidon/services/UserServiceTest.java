/*
 * Copyright (c) 2018.
 * By Alekum. All Rights Reserved.
 */

package org.helidon.services;

import io.helidon.webserver.WebServer;
import org.json.JSONObject;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.helidon.Launcher;
import org.helidon.Utils.WebUtils;

import javax.json.Json;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

@RunWith(JUnit4.class)
public class UserServiceTest
{
    private static final Logger logger = Logger.getLogger(UserServiceTest.class.getName());

    private static WebServer webServer;
    private static String location;

    @BeforeClass
    public static void beforeClass() throws Exception
    {
        webServer = Launcher.runWebServer(null);

        while(!webServer.isRunning())
        {
            Thread.sleep(1000);
        }

        location = "http://" + webServer.configuration().bindAddress().getCanonicalHostName()
                + ":" + webServer.port() + "/api";

        logger.info("WSLocation:" + location);
    }

    @AfterClass
    public static void afterClass() throws Exception
    {
        if(webServer != null)
        {
            webServer.shutdown().toCompletableFuture()
                    .get(1, TimeUnit.SECONDS);
        }
    }

    /*
     * User Created
     * POST /api/users
     * {"name": "<name>"}
     */
    @Test
    public void testPostUsers() throws Exception
    {
        String username = "Tony Stark";

        JSONObject result = WebUtils.doPost(location,
                                            "users",
                                            Json.createObjectBuilder().add("name", username).build());

        Assert.assertNotNull(result);
        Assert.assertTrue(result.has("userid"));
        Assert.assertEquals(result.getString("name"), username);

        logger.info("POST: created: " + result.toString());

        JSONObject resultW = WebUtils.doPost(location,
                                            "users",
                                             Json.createObjectBuilder().add("noname", "Tony Stark").build());

        Assert.assertNull(resultW);
        logger.info("POST: conflict: " + resultW);
    }

    /*
     * Get Users
     * GET /api/users
     */
    @Test
    public void testGetUsers() throws Exception
    {
        JSONObject result = WebUtils.doGet(location, "users","");

        Assert.assertNotNull(result);
        Assert.assertTrue(result.has("users"));
        Assert.assertNotNull(result.getJSONArray("users"));

        // @TODO(alekum) @Incomplete:
        // Check the users json array, it contains an empty array for test itself,
        // though might contain user from previous test.

        logger.info("GET: users: " + result.getString("users"));
    }

    /*
     * Delete User
     * DELETE /api/users/<userid>
     */
    @Test
    public void testDeleteUser() throws Exception
    {
        JSONObject deleted = WebUtils.doDelete(location, "users", UUID.randomUUID().toString());

        // @Information(alekum):
        // There is no user with the given UUID, that is why it returns empty result
        // and logger shows up null reference.
        //
        // @Cleanup return response rather than json object. It lets to check response codes.
        Assert.assertNull(deleted);

        logger.info("DELETE: conflict: " + deleted);

        // Create user with the given name
        String username = "Batman";
        JSONObject User = WebUtils.doPost(location,
                                          "users",
                                          Json.createObjectBuilder().add("name", username).build());

        Assert.assertNotNull(User);
        Assert.assertTrue(User.has("userid") & User.has("name"));
        Assert.assertEquals(username, User.getString("name"));
        logger.info("DELETE: created user: " + User.toString());

        // delete that user by uuid
        JSONObject deletedUser = WebUtils.doDelete(location, "users", User.getString("userid"));

        Assert.assertNotNull(deletedUser);
        Assert.assertEquals(User.getString("userid"), deletedUser.getString("userid"));
        Assert.assertEquals(User.getString("name"), deletedUser.getString("name"));
        logger.info("DELETED: deleted user: " + deletedUser.toString());

        // trying to get that user by uuid, must get empty result
        JSONObject expectedUser = WebUtils.doGet(location, "users", deletedUser.getString("userid"));
        Assert.assertNull(expectedUser);

        logger.info("DELETED: get deleted user: " + expectedUser);
    }

    /*
     * Change Username
     * PUT /api/users/<>
     * { "name": "<newname>"}
     *
     * Get User by UserID
     * GET /api/users/<userid>
     */
    @Test
    public void testUpdateUser() throws Exception
    {

        String username = "Tony Stark";

        JSONObject result = WebUtils.doPost(location,
                                            "users",
                                            Json.createObjectBuilder().add("name", username).build());

        Assert.assertNotNull(result);
        Assert.assertEquals(username, result.getString("name"));
        Assert.assertTrue(result.has("userid"));
        logger.info("PUT: created(POST): " + result.toString());

        String uuid = result.getString("userid");

        JSONObject user = WebUtils.doGet(location,
                                          "users",
                                          uuid);

        Assert.assertNotNull(user);
        Assert.assertEquals(result.getString("userid"), user.getString("userid"));
        Assert.assertEquals(result.getString("name"), user.getString("name"));
        logger.info("PUT: users(GET): " + user.toString());


        String updatedName = "Iron Man";
        JSONObject updated = WebUtils.doPut(location,
                                            "users",
                                            uuid,
                                            Json.createObjectBuilder().add("name" , updatedName).build());

        Assert.assertNotNull(updated);
        Assert.assertEquals(user.getString("userid"), updated.getString("userid"));
        Assert.assertNotEquals(user.getString("name"), updated.getString("name"));
        Assert.assertEquals(updated.getString("name"), updatedName);
        logger.info("PUT: updated(PUT): " + updated.toString());
    }
}
