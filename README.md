# Helidon Test Demo

## Dependencies

* Java8_181
* Gradle v 4.0.1
* Helidon v0.10.0
* jUnit v4.12
* Goebl v1.3.0

> Developed on Ubuntu Linux 17.10, Intellij IDEA - 2018.2

## Build 

* gradle build

## Test

* gradle test

> It runs internal tests where I used a web client to requests resources
> from the web server 
>
> For the record, the client uses different entities to represent the body
> of the response for success and failed ones. That's why for the sake of clarity
> the object in the response must be checked on null in order to operate the response
> properly. On other hand to make it more nicer the best way is to check the response
> code and getting the proper entity. 

* java -jar java -jar published/helidon-test-0.1.0-SNAPSHOT-all.jar

> It runs on localhost:8888 then you can just open it in your fav browser(CTRL+MouseClick)
> Read! Better to read first paragraph to figure out for what reason it has been created.
> The website was kindly provided by the internet and modestly moderated by me. 

##### Steps to play with the service from the front-end:
1. Pick a name and create user
2. Press Create user
3. Find user UUID in the section USERS
4. Copy\Paste into Create Account input field
5  Press Create Account
6. Find account UUID in the section ACCOUNTS
7. Copy\Paste into Transfer FROM , TO
8. Type some numbers you would like to have in AMOUNT
9. Press TRANSFER

> Disclamer, You will see some alerts notifications which will help you to get
what's going on in order to help you out a little with all this mess. 
> Nevertheless, I omit some checks. 

## Limitations

> It has a lot of limitations

0. not catching errors, though you can still see them (they handled by server anyway) in web dev console
0. cannot withdraw money
1. name can be empty
2. first you have to deposit money on account
3. cannot let you delete or change smth if it is run from a browser
4. etc.... 

## Publish 

* gradle uploadArchives
  * shadowJar
  * fatJar
  * Jar
