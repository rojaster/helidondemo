/*
 * Copyright (c) 2018.
 * By Alekum. All Rights Reserved.
 */

package org.helidon.services;

import io.helidon.webserver.ServerRequest;
import io.helidon.webserver.ServerResponse;

public interface IServiceHandlers
{
    void listHandler(ServerRequest req, ServerResponse res);
    void getHandler(ServerRequest req, ServerResponse res);
    void createHandler(ServerRequest req, ServerResponse res);
    void updateHandler(ServerRequest req, ServerResponse res);
    void deleteHandler(ServerRequest req, ServerResponse res);
}
