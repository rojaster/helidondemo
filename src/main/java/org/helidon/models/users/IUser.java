/*
 * Copyright (c) 2018.
 * By Alekum. All Rights Reserved.
 */

package org.helidon.models.users;

import java.util.Set;
import java.util.UUID;

public interface IUser
{
    /*
     * @return void
     */
    void setUserName(String name);

    /*
     *  @return username
     */
    String getUserName();

    /*
     *  @return user id
     */
    UUID getUserID();

    /*
     * @return set of accounts ids associated with the user
     */
    Set<UUID> accountsIDs();
}
