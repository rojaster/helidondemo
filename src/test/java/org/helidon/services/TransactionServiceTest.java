/*
 * Copyright (c) 2018.
 * By Alekum. All Rights Reserved.
 */

package org.helidon.services;

import io.helidon.webserver.WebServer;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.helidon.Launcher;
import org.helidon.Utils.WebUtils;
import org.helidon.models.transactions.TransactionStatus;

import javax.json.Json;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

@RunWith(JUnit4.class)
public class TransactionServiceTest
{
    private final static Logger logger = Logger.getLogger(TransactionServiceTest.class.getName());;

    private static WebServer webServer;
    private static String location;

    private static List<String> usersIDs;
    private static List<String> accountsIDs;

    @BeforeClass
    public static void beforeClass() throws Exception
    {
        webServer = Launcher.runWebServer(null);

        while(!webServer.isRunning())
        {
            Thread.sleep(1000);
        }

        location = "http://" + webServer.configuration().bindAddress().getCanonicalHostName()
                + ":" + webServer.port() + "/api";

        logger.info("WSLocation:" + location);

        usersIDs = Arrays.asList(createUser("Thor"),
                                 createUser("Hulk"),
                                 createUser("Romanoff"),
                                 createUser("IronMan"));

        accountsIDs = new ArrayList<>();
        for(String usersID : usersIDs)
        {
            String account = createAccount(usersID);
            accountsIDs.add(account);
        }
    }

    @AfterClass
    public static void afterClass() throws Exception
    {
        if(webServer != null)
        {
            webServer.shutdown().toCompletableFuture().get(1, TimeUnit.SECONDS);
        }

        usersIDs = null;
        accountsIDs = null;
    }

    /*
     * Create transaction but not proceed
     * It has an assigned status - PROCESSING.
     *
     * POST /api/transactions
     * {"name": "<name>"}
     */
    @Test
    public void testPostTransaction() throws Exception
    {
        JSONObject transaction = WebUtils.doPost(location,
                                                 "transactions",
                                                 Json.createObjectBuilder()
                                                         .add("from", accountsIDs.get(0))
                                                         .add("to", accountsIDs.get(0))
                                                         .add("amount", "10").build());
        Assert.assertNotNull(transaction);
        Assert.assertEquals("10", transaction.getString("amount"));
        Assert.assertEquals(accountsIDs.get(0), transaction.getString("from"));
        Assert.assertEquals(accountsIDs.get(0), transaction.getString("to"));
        Assert.assertTrue(transaction.has("transactionid"));

        logger.info("POST: transact created: " + transaction.toString());
    }

    @Test
    /*
     * Proceed Transaction
     * PUT /api/transactions/<transactionid>
     */
    public void testPutTransactions() throws Exception
    {
        BigDecimal amounts[] = {new BigDecimal("100000"),
                new BigDecimal("10000"),
                new BigDecimal("-19999"),};

        TransactionStatus statuses[] = {TransactionStatus.COMPLETE,  // ok
                                        TransactionStatus.COMPLETE,  // ok
                                        TransactionStatus.DECLINED}; // negative amount is forbidden

        JSONObject transaction = WebUtils.doPost(location,
                                                 "transactions",
                                                 Json.createObjectBuilder()
                                                         .add("from", accountsIDs.get(0))
                                                         .add("to", accountsIDs.get(0))
                                                         .add("amount", amounts[0].toString()).build());
        Assert.assertNotNull(transaction);
        Assert.assertEquals(amounts[0], new BigDecimal(transaction.getString("amount")));
        Assert.assertEquals(accountsIDs.get(0), transaction.getString("from"));
        Assert.assertEquals(accountsIDs.get(0), transaction.getString("to"));
        Assert.assertTrue(transaction.has("transactionid"));

        logger.info("PUT: transact created: " + transaction.toString());

        JSONObject transaction1 = WebUtils.doPost(location,
                                                  "transactions",
                                                  Json.createObjectBuilder()
                                                          .add("from", accountsIDs.get(1))
                                                          .add("to", accountsIDs.get(1))
                                                          .add("amount", amounts[1].toString()).build());
        Assert.assertNotNull(transaction1);
        Assert.assertEquals(amounts[1], new BigDecimal(transaction1.getString("amount")));
        Assert.assertEquals(accountsIDs.get(1), transaction1.getString("from"));
        Assert.assertEquals(accountsIDs.get(1), transaction1.getString("to"));
        Assert.assertTrue(transaction.has("transactionid"));

        logger.info("PUT: transact created: " + transaction1.toString());


        JSONObject transaction2 = WebUtils.doPost(location,
                                                  "transactions",
                                                  Json.createObjectBuilder()
                                                          .add("from", accountsIDs.get(0))
                                                          .add("to", accountsIDs.get(1))
                                                          .add("amount", amounts[2].toString()).build());
        Assert.assertNotNull(transaction2);
        Assert.assertEquals(amounts[2], new BigDecimal(transaction2.getString("amount")));
        Assert.assertEquals(accountsIDs.get(0), transaction2.getString("from"));
        Assert.assertEquals(accountsIDs.get(1), transaction2.getString("to"));
        Assert.assertTrue(transaction2.has("transactionid"));

        logger.info("PUT: transact created: " + transaction2.toString());


        JSONObject proceed = WebUtils.doPut(location, "transactions",
                                            transaction.getString("transactionid"),
                                            Json.createObjectBuilder().build());
        Assert.assertNotNull(proceed);
        Assert.assertEquals(transaction.getString("transactionid"), proceed.getString("transactionid"));
        Assert.assertNotEquals(transaction.getString("status"), proceed.getString("status"));
        Assert.assertEquals(statuses[0].toString(), proceed.getString("status"));

        logger.info("PUT: transact proceeded: " + proceed.toString());

        JSONObject proceed1 = WebUtils.doPut(location, "transactions",
                                             transaction1.getString("transactionid"),
                                             Json.createObjectBuilder().build());
        Assert.assertNotNull(proceed);
        Assert.assertEquals(transaction1.getString("transactionid"), proceed1.getString("transactionid"));
        Assert.assertNotEquals(transaction1.getString("status"), proceed1.getString("status"));
        Assert.assertEquals(statuses[1].toString(), proceed1.getString("status"));

        logger.info("PUT: transact proceeded: " + proceed1.toString());

        JSONObject proceed2 = WebUtils.doPut(location, "transactions",
                                             transaction2.getString("transactionid"),
                                             Json.createObjectBuilder().build());
        Assert.assertNull(proceed2);

        proceed2 = WebUtils.doGet(location, "transactions", transaction2.getString("transactionid"));

        Assert.assertEquals(transaction2.getString("transactionid"), proceed2.getString("transactionid"));
        Assert.assertNotEquals(transaction2.getString("status"), proceed2.getString("status"));
        Assert.assertEquals(statuses[2].toString(), proceed2.getString("status"));

        logger.info("PUT: transact proceeded: " + proceed2.toString());

        JSONObject account0 = WebUtils.doGet(location, "accounts", accountsIDs.get(0));
        Assert.assertNotNull(account0);
        Assert.assertEquals(accountsIDs.get(0), account0.getString("accountid"));
        Assert.assertEquals(amounts[0].toString(), account0.getString("balance"));

        logger.info("PUT: account 0: " + account0.toString());

        JSONObject account1 = WebUtils.doGet(location, "accounts", accountsIDs.get(1));
        Assert.assertNotNull(account1);
        Assert.assertEquals(accountsIDs.get(1), account1.getString("accountid"));
        Assert.assertEquals(amounts[1].toString(), account1.getString("balance"));

        logger.info("PUT: account 1: " + account1.toString());

    }

    /*
     * Get Transactions
     * GET /api/transactions
     */
    @Test
    public void testGetTransactions() throws Exception
    {
        JSONObject transactions = WebUtils.doGet(location, "transactions","");

        Assert.assertNotNull(transactions);
        Assert.assertTrue(transactions.has("transactions"));
        Assert.assertTrue(transactions.getJSONArray("transactions").length() >= 0);

        // @Cleanup(alekum):
        // Make it more robust. Post new transaction and check if it is in a returned array.
        // Cause Array is empty for single test, but not empty for the whole test.

        logger.info("GET: transactions: " + transactions.toString());
    }

    /*
     * Get Transaction by transactionID
     * GET /api/transactions/<transactionid>
     */
    @Test
    public void testGetTransaction() throws Exception
    {
        JSONObject transactionNotExist = WebUtils.doGet(location, "transactions", UUID.randomUUID().toString());
        Assert.assertNull(transactionNotExist);

        JSONObject transaction = WebUtils.doPost(location, "transactions",
                                                 Json.createObjectBuilder()
                                                         .add("from", accountsIDs.get(0))
                                                         .add("to", accountsIDs.get(1))
                                                         .add("amount", "10").build());
        Assert.assertNotNull(transaction);
        Assert.assertTrue(transaction.has("transactionid"));

        JSONObject get = WebUtils.doGet(location, "transactions", transaction.getString("transactionid"));
        Assert.assertNotNull(get);
        Assert.assertEquals(transaction.getString("from"), get.getString("from"));
        Assert.assertEquals(transaction.getString("to"), get.getString("to"));
        Assert.assertEquals(transaction.getString("amount"), get.getString("amount"));
        Assert.assertEquals(transaction.getString("transactionid"), get.getString("transactionid"));

        logger.info("GET: transactions: " + transaction.toString());
    }

    /*
     * Delete Transaction
     * DELETE /api/transactions/<transactionid>
     */
    @Test
    public void testDeleteTransaction() throws Exception
    {
        JSONObject transaction = WebUtils.doPost(location, "transactions",
                                                 Json.createObjectBuilder()
                                                         .add("from", accountsIDs.get(0))
                                                         .add("to", accountsIDs.get(1))
                                                         .add("amount", "1").build());

        Assert.assertNotNull(transaction);
        Assert.assertEquals(accountsIDs.get(0), transaction.getString("from"));
        Assert.assertEquals(accountsIDs.get(1), transaction.getString("to"));
        Assert.assertEquals(TransactionStatus.PROCESSING.toString(), transaction.getString("status"));
        Assert.assertTrue(transaction.has("transactionid"));

        logger.info("DELETE: transaction created: " + transaction.toString());

        JSONObject deleted = WebUtils.doDelete(location, "transactions", transaction.getString("transactionid"));

        Assert.assertNotNull(deleted);
        Assert.assertEquals(transaction.getString("transactionid"), deleted.getString("transactionid"));
        Assert.assertEquals(transaction.getString("from"), deleted.getString("from"));
        Assert.assertEquals(transaction.getString("to"), deleted.getString("to"));
        Assert.assertEquals(transaction.getString("status"), deleted.getString("status"));
        Assert.assertEquals(transaction.getString("amount"), deleted.getString("amount"));

        logger.info("DELETE: transaction deleted: " + deleted.toString());

        JSONObject get = WebUtils.doGet(location, "transaction", deleted.getString("transactionid"));
        Assert.assertNull(get);

        logger.info("DELETE: get deleted transaction: " + get);
    }

    private static String createUser(String name) throws Exception
    {
        JSONObject result = WebUtils.doPost(location,
                                            "users",
                                            Json.createObjectBuilder().add("name", name).build());

        logger.info("POST: user created: " + result.toString());

        return result.getString("userid");
    }

    private static String createAccount(String userID) throws Exception
    {
        JSONObject result = WebUtils.doPost(location,
                                            "accounts",
                                            Json.createObjectBuilder().add("userid", userID).build());

        logger.info("POST: account created: " + result.toString());

        return result.getString("accountid");
    }
}
