/*
 * Copyright (c) 2018.
 * By Alekum. All Rights Reserved.
 */

package org.helidon.storages.inmemory;

import org.helidon.models.accounts.IAccount;

import java.util.HashMap;
import java.util.UUID;

public class AccountsStorage<K extends UUID, V extends IAccount> extends HashMap<K, V>
{
}
