/*
 * Copyright (c) 2018.
 * By Alekum. All Rights Reserved.
 */

package org.helidon.storages.inmemory;

import org.helidon.models.users.IUser;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class UsersStorage<K extends UUID, V extends IUser> extends HashMap<K, V>
{}
