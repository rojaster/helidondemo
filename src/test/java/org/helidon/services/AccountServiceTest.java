/*
 * Copyright (c) 2018.
 * By Alekum. All Rights Reserved.
 */

package org.helidon.services;

import io.helidon.webserver.WebServer;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.helidon.Launcher;
import org.helidon.Utils.WebUtils;

import javax.json.Json;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RunWith(JUnit4.class)
public class AccountServiceTest
{
    private final static Logger logger = Logger.getLogger(AccountServiceTest.class.getName());

    private static WebServer webServer;
    private static String location;
    private static List<String> usersIDs;

    @BeforeClass
    public static void beforeClass() throws Exception
    {
        webServer = Launcher.runWebServer(null);

        while(!webServer.isRunning())
        {
            Thread.sleep(1000);
        }

        location = "http://" + webServer.configuration().bindAddress().getCanonicalHostName()
                + ":" + webServer.port() + "/api";

        logger.info("WSLocation:" + location);


        // @Cleanup(alekum):
        // It is not a most convenient way of creating users, though, It works for testing purposes.
        // Though, It would be better to refactor this.
        usersIDs = Stream.of("Hulk", "Thor", "Iron Man")
                        .map(name -> {
                             JSONObject json = WebUtils.doPost(location,
                                                        "users",
                                                                Json.createObjectBuilder()
                                                                       .add("name", name)
                                                                       .build());
                             logger.info("POST: user created(POST): " + json.toString());

                             try
                             {
                                 return json.getString("userid");
                             }
                             catch(JSONException e)
                             {
                                 return "";
                             }
                        })
                        .collect(Collectors.toList());
    }

    @AfterClass
    public static void afterClass() throws Exception
    {
        if(webServer != null)
        {
            webServer.shutdown().toCompletableFuture()
                    .get(1, TimeUnit.SECONDS);
        }

        usersIDs.clear();
        usersIDs = null;
    }

    /*
     * Account Created
     * POST /api/accounts
     * {"name": "<name>"}
     */
    @Test
    public void testPostAccount() throws Exception
    {
        JSONObject result;

        for(String userID : usersIDs)
        {
            result = WebUtils.doPost(location,
                             "accounts",
                                     Json.createObjectBuilder().add("userid", userID).build());

            Assert.assertNotNull(result);
            Assert.assertTrue(result.has("accountid"));
            Assert.assertTrue(!result.getString("accountid").isEmpty());
            Assert.assertEquals(userID, result.getString("userid"));
            logger.info("POST: created: " + result.toString());
        }

        // Negative result, it is null if something goes wrong on server.
        // For the record, server tries to find user and if it can't find it, returns NOT FOUND and null result.
        // More precisely, the client library, that I use, returns a null result. The actual code is stored in a ErrorBody though.
        result = WebUtils.doPost(location, "accounts",
                                 Json.createObjectBuilder().add("userid", UUID.randomUUID().toString()).build());
        Assert.assertNull(result);
    }

    /*
     * Get Accounts
     * GET /api/accounts
     */
    @Test
    public void testGetAccounts() throws Exception
    {
        JSONObject accounts = WebUtils.doGet(location, "accounts","");

        Assert.assertNotNull(accounts);
        Assert.assertTrue(accounts.has("accounts"));

        // @Cleanup(alekum):
        // Better case to check created users and their accounts that have been created already in previous test.
        // However, if this test is run as a single test, it's received an empty array of accounts.
        Assert.assertTrue(accounts.getJSONArray("accounts").length() >= 0);

        logger.info("GET: users: " + accounts.getString("accounts"));
    }

    /*
     * Delete Account
     * DELETE /api/accounts/<userid>
     */
    @Test
    public void testDeleteAccount() throws Exception
    {
        JSONObject account = WebUtils.doDelete(location, "accounts", UUID.randomUUID().toString());

        Assert.assertNull(account);
        logger.info("DELETE: conflict: " + account);

        JSONObject newAccount = WebUtils.doPost(location, "accounts",
                                                Json.createObjectBuilder().add("userid", usersIDs.get(0)).build());
        Assert.assertNotNull(newAccount);
        Assert.assertEquals(usersIDs.get(0), newAccount.getString("userid"));
        logger.info("DELETE: created account: " + newAccount);

        JSONObject deletedAccount = WebUtils.doDelete(location, "accounts", newAccount.getString("accountid"));
        Assert.assertNotNull(deletedAccount);
        Assert.assertEquals(newAccount.getString("accountid"), deletedAccount.getString("accountid"));
        Assert.assertEquals(newAccount.getString("userid"), deletedAccount.getString("userid"));
        logger.info("DELETE: deleted Account: " + deletedAccount);

        JSONObject getAccount = WebUtils.doGet(location, "accounts", deletedAccount.getString("accountid"));
        Assert.assertNull(getAccount);
        logger.info("DELETE: get account: " + getAccount);
    }

    /*
     * Get Account by AccountID
     * GET /api/accounts/<accountid>
     */
    @Test
    public void testGetAccount() throws Exception
    {
        String userID = usersIDs.get(2);

        JSONObject result = WebUtils.doPost(location,
                                            "accounts",
                                            Json.createObjectBuilder().add("userid", userID).build());

        Assert.assertNotNull(result);
        Assert.assertEquals(userID, result.getString("userid"));
        logger.info("GET: created(POST): " + result.toString());

        String uuid = result.getString("accountid");

        JSONObject accounts = WebUtils.doGet(location, "accounts","");

        Assert.assertNotNull(accounts);
        Assert.assertTrue(accounts.has("accounts"));
        Assert.assertTrue(accounts.getJSONArray("accounts").length() > 0);
        logger.info("GET: accounts(GET): " + accounts.toString());

        JSONObject account = WebUtils.doGet(location, "accounts", uuid);

        Assert.assertNotNull(account);
        Assert.assertEquals(uuid, account.getString("accountid"));
        Assert.assertEquals(userID, account.getString("userid"));
        logger.info("GET: account(GET): " + account.toString());
    }
}
