/*
 * Copyright (c) 2018.
 * By Alekum. All Rights Reserved.
 */

package org.helidon.models.users;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

/*
* @Information(alekum):
* Some developers find it irritated in order of obeying OOP.
* But I would remove here interface implementation
 */
public class User implements IUser
{
    private final UUID userID;
    private String name;

    private final Set<UUID> accountsIDs;

    public User(UUID _uuid, String _name)
    {
        this.userID = _uuid;
        this.name   = _name;

        this.accountsIDs = new HashSet<>();
    }

    public User(String _name)
    {
        this(UUID.randomUUID(), _name);
    }

    @Override
    public int hashCode()
    {
        return this.userID.hashCode() & super.hashCode();
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj instanceof User)
        {
            User rhs = (User) obj;
            return this.userID.equals(rhs.getUserID());
        }
        return false;
    }

    @Override
    public void setUserName(String _name)
    {
        this.name = _name;
    }

    @Override
    public String getUserName()
    {
        return this.name;
    }

    @Override
    public UUID getUserID()
    {
        return this.userID;
    }

    @Override
    public Set<UUID> accountsIDs()
    {
        return this.accountsIDs;
    }
}
