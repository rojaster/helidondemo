/*
 * Copyright (c) 2018.
 * By Alekum. All Rights Reserved.
 */

// It's been too long since we met at first time JQuery=)
// this is just wip to proof that it works how it is supposed to work
// no generalization, no refactoring, dirty hacks
$(document).ready(function(){

    var updateList = function(listType) {
        var settings = {
            "async": true,
            "crossDomain": true,
            "url": "http://localhost:8888/api/" + listType,
            "method": "GET",
            "headers": {}
        };

        $.ajax(settings).done(function(response) {
            var list = response[listType];

            $("#"+listType)
                .empty()
                .append(list.map(function(u){return "<p>" + JSON.stringify(u) + "</p>"}));

            console.log(response);
        });
    };

    var updateUsers = function() {
        updateList("users");
    };

    var updateAccounts = function() {
        updateUsers();
        updateList("accounts");
    };

    var updateTransactions = function() {
        updateAccounts();
        updateList("transactions")
    };

    var createEntity = function(entityType, dataObject, updateFunctions, actionFunctions) {
        actionFunctions = actionFunctions || [];
        updateFunctions = updateFunctions || [];

        var settings = {
            "async": true,
            "crossDomain": true,
            "url": "http://localhost:8888/api/" + entityType,
            "method": "POST",
            "headers": {},
            "data": JSON.stringify(dataObject)
        };

        $.ajax(settings).done(function(response) {
            alert("Created: " + JSON.stringify(response));
            console.log(response);

            updateFunctions.map(function(f){return f();});
            actionFunctions.map(function(f){return f(response);});
            updateFunctions.map(function(f){return f();});
        });
    };

    var transferFunction = function(transactionObject) {
        var tID = transactionObject["transactionid"];

        alert("Transferring:" + tID);

        var settings = {
            "async": true,
            "crossDomain": true,
            "url": "http://localhost:8888/api/transactions/" + tID,
            "method": "PUT",
            "headers": {}
        };

        $.ajax(settings)
            .fail(function(jqXHR,textStatus,errorThrown ){
                alert(textStatus);
                console.log(errorThrown);
            })
            .done(function (response, textStatus) {
                alert("Proceed " + textStatus);
                console.log(response);
            });
    };

    $("#create_user").click(function(){
        var username = $("#username").val().trim();
        createEntity("users", {name: username}, [updateUsers]);
    });

    $("#create_acc").click(function(){
        var userid = $("#userid").val().trim();
        createEntity("accounts", { userid: userid }, [updateAccounts]);
    });

    $("#transfer").click(function(){
        var from   = $("#fromAccount").val().trim();
        var to     = $("#toAccount").val().trim();
        var amount = $("#Amount").val().trim();
        createEntity("transactions", {from: from, to: to, amount: amount}, [updateTransactions], [transferFunction])
    });
});
