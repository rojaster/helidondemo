/*
 * Copyright (c) 2018.
 * By Alekum. All Rights Reserved.
 */

package org.helidon;


import io.helidon.common.http.Http;
import io.helidon.common.http.MediaType;
import io.helidon.config.Config;
import io.helidon.config.ConfigSources;
import io.helidon.webserver.*;
import io.helidon.webserver.json.JsonSupport;
import org.helidon.dao.EntityManager;
import org.helidon.dao.IEntityManager;
import org.helidon.messages.ContextMessages;
import org.helidon.models.accounts.IAccount;
import org.helidon.models.transactions.Transaction;
import org.helidon.models.users.IUser;
import org.helidon.services.AccountService;
import org.helidon.services.TransactionService;
import org.helidon.services.UserService;
import org.helidon.storages.inmemory.AccountsStorage;
import org.helidon.storages.inmemory.TransactionsStorage;
import org.helidon.storages.inmemory.UsersStorage;

import javax.json.Json;
import javax.json.JsonObject;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Launcher
{
    private static final Logger logger = Logger.getLogger(Launcher.class.getName());

    public static void main(String[] args)
    {
        ServerConfiguration serverConfiguration = null;

        if(args != null && 2 == args.length && (args[0].equals("-p") || args[0].equals("--port")))
        {
            System.out.println("HELIDON! - BANG!");
            try
            {
                int port = Integer.parseInt(args[1]);
                serverConfiguration = ServerConfiguration.builder()
                        .port(port)
                        .build();
            }
            catch(NumberFormatException nfe)
            {
                logger.log(Level.SEVERE, "port: " + args[1] + " could not be proceed correctly");
                System.exit(-1);
            }
        }

        WebServer webServer = runWebServer(serverConfiguration);

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {

            webServer.shutdown().thenRunAsync(() -> {logger.info("Bye");});

        }));
    }

    public static WebServer runWebServer(ServerConfiguration serverConfiguration)
    {
        IEntityManager<UUID,
                UsersStorage<UUID, IUser>,
                AccountsStorage<UUID, IAccount>,
                TransactionsStorage<UUID, Transaction>> entityManager = new EntityManager<>(new UsersStorage<>(),
                                                                                            new AccountsStorage<>(),
                                                                                            new TransactionsStorage<>());

        UserService userService = new UserService(entityManager);
        AccountService accountService = new AccountService(entityManager);
        TransactionService transactionService = new TransactionService(entityManager);

        return runWebServer(serverConfiguration,
                                            userService, accountService, transactionService);
    }

    public static WebServer runWebServer(ServerConfiguration serverConfiguration,
                                         Service userService,
                                         Service accountService,
                                         Service transactionService)
    {
        if(null == serverConfiguration)
        {
            Config serverConfig = Config.builder()
                    .sources(ConfigSources.classpath("application.yaml"))
                    .build();

            serverConfiguration = ServerConfiguration.fromConfig(serverConfig);
        }

        Routing serverRouting =  Routing.builder()
                .register("/img", StaticContentSupport.create("static/img"))
                .register("/css", StaticContentSupport.create("static/css"))
                .register("/js", StaticContentSupport.create("static/js"))
                .register("/favicon.ico", StaticContentSupport.create("static/favicon.ico"))
                .register("/", StaticContentSupport.builder("static")
                        .welcomeFileName("index.html").build())

                .register(JsonSupport.get())
                .register("/api", userService, accountService, transactionService)

                .error(NotFoundException.class, (request, response, exception ) -> {
                    response.headers().contentType(MediaType.APPLICATION_JSON);
                    response.status(Http.Status.NOT_FOUND_404);
                    response.send(Json.createArrayBuilder().build());
                })
                .error(HttpException.class, (request, response, exception) -> {
                    if(exception.status().code() == 304)
                    {
                        response.status(Http.Status.NOT_MODIFIED_304);
                        logger.warning(String.format("Code: %s, Phrase: %s, Family: %s",
                                                     exception.status().code(),
                                                     exception.status().reasonPhrase(),
                                                     exception.status().family().name()));
                        response.send();
                    }
                    else
                    {
                        request.next(new Exception(exception.getMessage()));
                    }
                })
                .error(Exception.class, (request, response, exception) -> {
                    response.headers().contentType(MediaType.APPLICATION_JSON);
                    response.status(Http.Status.BAD_REQUEST_400);

                    JsonObject result = Json.createObjectBuilder()
                            .add("message",
                                 String.format("%s", ContextMessages.BAD_REQUEST_MESSAGE_RESPONSE))
                            .build();

                    response.send(result);

                    logger.log(Level.SEVERE, ContextMessages.BAD_REQUEST_MESSAGE_RESPONSE
                            + " : " + request.uri().getPath() + " : " + request.method().name());
                    logger.log(Level.SEVERE, "Exception: " + exception.toString() + "\n " + exception.getCause());
                })
                .build();

        WebServer webServer = WebServer.create(serverConfiguration, serverRouting);

        webServer.start()
                .thenAccept(ws -> logger.info("Server started at: http://"
                                                      + webServer.configuration().bindAddress().getCanonicalHostName()
                                                      + ":"
                                                      + ws.port()
                                                      + "/"));

        webServer.whenShutdown().thenRunAsync(() -> {
            logger.info("Tango down!");
            logger.info("“The future was uncertain, absolutely, and there were many hurdles, " +
                                "twists, and turns to come, but as long as I kept moving forward, " +
                                "one foot in front of the other, the voices of fear and shame, " +
                                "the messages from those who wanted me to believe that " +
                                "I wasn't good enough, would be stilled.” \n" +
                                "― Chris Gardner, The Pursuit of Happyness\n\n");
        });

        return webServer;
    }
}
