/*
 * Copyright (c) 2018.
 * By Alekum. All Rights Reserved.
 */

package org.helidon.Utils;

import com.goebl.david.Response;
import com.goebl.david.Webb;
import org.json.JSONObject;

import javax.json.Json;
import javax.json.JsonObject;
import java.util.UUID;

public class WebUtils
{
    public static JSONObject doPost(String location, String service, JsonObject body)
    {
        Response<JSONObject> response = Webb.create()
                .post(location + "/" + service)
                .header("Content-Type", "application/json")
                .body(body)
                .asJsonObject();

        response.getConnection().disconnect();

        return response.getBody();
    }

    public static JSONObject doGet(String location, String service, String uuid)
    {
        Response<JSONObject> response = Webb.create()
                .get(location + "/" + service + "/" + uuid)
                .asJsonObject();

        response.getConnection().disconnect();

        return response.getBody();
    }

    public static JSONObject doDelete(String location, String service, String uuid)
    {
        Response<JSONObject> response = Webb.create()
                .delete(location + "/" + service + "/" + uuid)
                .asJsonObject();
        response.getConnection().disconnect();

        return response.getBody();
    }

    public static JSONObject doPut(String location, String service, String uuid, JsonObject body)
    {
        Response<JSONObject> response = Webb.create()
                .put(location + "/" + service + "/" + uuid)
                .header("Content-Type", "application/json")
                .body(body)
                .asJsonObject();
        response.getConnection().disconnect();

        return response.getBody();
    }
}
