/*
 * Copyright (c) 2018.
 * By Alekum. All Rights Reserved.
 */

package org.helidon.services;

import io.helidon.common.http.Http;
import io.helidon.common.http.MediaType;
import io.helidon.webserver.*;
import org.helidon.dao.IEntityManager;
import org.helidon.messages.ContextMessages;
import org.helidon.models.accounts.IAccount;
import org.helidon.models.transactions.Transaction;
import org.helidon.models.users.IUser;
import org.helidon.models.users.User;
import org.helidon.storages.inmemory.AccountsStorage;
import org.helidon.storages.inmemory.TransactionsStorage;
import org.helidon.storages.inmemory.UsersStorage;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import java.util.Collection;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;


public class UserService extends AbstractService implements Service, IServiceHandlers
{
    private static final Logger logger = Logger.getLogger(UserService.class.getName());

    public UserService(IEntityManager<UUID,
                                        UsersStorage<UUID, IUser>,
                                        AccountsStorage<UUID, IAccount>,
                                        TransactionsStorage<UUID, Transaction>> _entityManager)
    {
        this.entityManager = _entityManager;
    }

    @Override
    public void update(Routing.Rules rules)
    {
        logger.info("UserService is Registered");

        rules.get("/users", this::listHandler)
                .get("/users/{userid}", this::getHandler)
                .post("/users", this::createHandler)
                .put("/users/{userid}", this::updateHandler)
                .delete("/users/{userid}", this::deleteHandler);
    }

    public synchronized void listHandler(ServerRequest req, ServerResponse res)
    {
            Collection<IUser> users = entityManager.users().values();
        JsonArrayBuilder builder = Json.createArrayBuilder();

        users.stream().map(UserService::makeUserJsonObject).forEach(builder::add);

        JsonObject result = Json.createObjectBuilder().add("users", builder.build()).build();

        res.status(Http.Status.OK_200).send(result);
    }

    public synchronized void getHandler(ServerRequest req, ServerResponse res)
    {
        IUser user = entityManager.users().get(UUID.fromString(req.path().param("userid")));

        Http.Status status;
        JsonObject result;


        if(user != null)
        {
            result = UserService.makeUserJsonObject(user);
            status = Http.Status.OK_200;
        }
        else
        {
            result = Json.createObjectBuilder().add("message", ContextMessages.USER_CANT_BE_FOUND).build();
            status = Http.Status.CONFLICT_409;
        }

        res.headers().contentType(MediaType.APPLICATION_JSON);
        res.status(status).send(result);
    }

    public synchronized void createHandler(ServerRequest req, ServerResponse res)
    {
        req.content().as(JsonObject.class).thenAccept(payload->{
            try
            {
                String name = payload.getString("name");
                IUser user = new User(name);
                entityManager.users().put(user.getUserID(), user);

                JsonObject result = UserService.makeUserJsonObject(user);;
                Http.Status status = Http.Status.CREATED_201;;

                res.headers().contentType(MediaType.APPLICATION_JSON);
                res.status(status).send(result);
            }
            catch(NullPointerException | ClassCastException e)
            {
                req.next(e);
            }
        });
    }

    public synchronized void updateHandler(ServerRequest req, ServerResponse res)
    {
        req.content().as(JsonObject.class).thenAccept(payload -> {
            try
            {
                UUID userID = UUID.fromString(req.path().param("userid"));
                String newUsername = payload.getString("name");

                IUser user = entityManager.users().get(userID);
                user.setUserName(newUsername);

                logger.log(Level.INFO, String.format("PUT payload: %s", payload.toString()));

                JsonObject result = UserService.makeUserJsonObject(user);
                Http.Status status = Http.Status.ACCEPTED_202;

                res.headers().contentType(MediaType.APPLICATION_JSON);
                res.status(status).send(result);
            }
            catch(NullPointerException | ClassCastException | IllegalArgumentException e)
            {
                req.next(new Exception(e.getMessage()));
            }
        });
    }

    public synchronized void deleteHandler(ServerRequest req, ServerResponse res)
    {
        IUser deletedUser = entityManager.users().get(UUID.fromString(req.path().param("userid")));

        JsonObject result;
        Http.Status status;

        if(deletedUser != null)
        {
            deletedUser.accountsIDs()
                    .parallelStream()
                    .map(entityManager.accounts()::get)
                    .forEach(account -> account.transactions().forEach(entityManager.transactions()::remove));

            result = UserService.makeUserJsonObject(deletedUser);

            deletedUser.accountsIDs().parallelStream().forEach(entityManager.accounts()::remove);
            entityManager.users().remove(deletedUser.getUserID());

            logger.log(Level.INFO, String.format("USER DELETED: %s", result.toString()));

            status = Http.Status.ACCEPTED_202;
        }
        else
        {
            status = Http.Status.CONFLICT_409;
            result = Json.createObjectBuilder().add("message", status.toString()).build();
        }

        res.headers().contentType(MediaType.APPLICATION_JSON);
        res.status(status).send(result);
    }

    // @Cleanup(alekum):
    // Perhaps this ought to be in another place where serialization/deserialization happens(in manager??)
    private static JsonObject makeUserJsonObject(IUser user)
    {
        JsonArrayBuilder builder = Json.createArrayBuilder();
        user.accountsIDs().parallelStream().map(UUID::toString).forEach(builder::add);

        return Json.createObjectBuilder()
                .add("userid", user.getUserID().toString())
                .add("name", user.getUserName())
                .add("accountsIds", builder.build())
                .build();
    }
}
