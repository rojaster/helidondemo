/*
 * Copyright (c) 2018.
 * By Alekum. All Rights Reserved.
 */

package org.helidon.services;

import io.helidon.webserver.Service;
import org.helidon.dao.IEntityManager;
import org.helidon.models.accounts.IAccount;
import org.helidon.models.transactions.Transaction;
import org.helidon.models.users.IUser;
import org.helidon.storages.inmemory.AccountsStorage;
import org.helidon.storages.inmemory.TransactionsStorage;
import org.helidon.storages.inmemory.UsersStorage;

import java.util.UUID;

abstract class AbstractService implements Service, IServiceHandlers
{
     IEntityManager<UUID,
                    UsersStorage<UUID, IUser>,
                    AccountsStorage<UUID, IAccount>,
                    TransactionsStorage<UUID, Transaction>> entityManager;
}
