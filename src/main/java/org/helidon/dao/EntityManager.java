 /*
 * Copyright (c) 2018.
 * By Alekum. All Rights Reserved.
 */

package org.helidon.dao;

import org.helidon.models.accounts.IAccount;
import org.helidon.models.transactions.Transaction;
import org.helidon.models.users.IUser;

import java.util.Map;
import java.util.UUID;

 /*
  * This class has been created for experiments and approaching different concept
  * to make it much closer to DOD. It cut some crap and
  * It satisfies my insatiable sense of curiosity that I have.
  */
public class EntityManager<K extends UUID,
                        U extends Map<K, IUser>,
                        A extends Map<K, IAccount>,
                        T extends Map<K, Transaction>> implements IEntityManager<K, U, A, T>
{
    private final U users;
    private final A accounts;
    private final T transactions;

    public EntityManager(U _users,
                         A _accounts,
                         T _transactions)
    {
        this.users = _users;
        this.accounts = _accounts;
        this.transactions = _transactions;
    }

    @Override
    public U users()
    {
        return this.users;
    }

    @Override
    public A accounts()
    {
        return this.accounts;
    }

    @Override
    public T transactions()
    {
        return this.transactions;
    }
}
