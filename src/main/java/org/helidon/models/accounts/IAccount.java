/*
 * Copyright (c) 2018.
 * By Alekum. All Rights Reserved.
 */

package org.helidon.models.accounts;

import java.math.BigDecimal;
import java.util.Set;
import java.util.UUID;

public interface IAccount
{
    /*
     *   Return account id
     *   @return account id
     */
    UUID getAccountID();

    /*
     *   Return Account Holder
     *   @return IUser
     */
    UUID getAccountHolderID();

    /*
     *   Return current balance
     *   @return balance
     */
    BigDecimal getAccountBalance();

    /*
     *   Set new balance
     */
    void setAccountBalance(BigDecimal balance);

    /*
     * Return set of transactions IDs associated with the account
     *
     * @return set of transactions IDs
     */
    Set<UUID> transactions();

}
