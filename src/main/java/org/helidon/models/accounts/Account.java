/*
 * Copyright (c) 2018.
 * By Alekum. All Rights Reserved.
 */

package org.helidon.models.accounts;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class Account implements IAccount
{
    private final UUID accountID;
    private final UUID userID;
    private BigDecimal balance;

    private final Set<UUID> transactions;

    public Account(UUID _userID)
    {
        this.userID      = _userID;
        this.balance   = new BigDecimal(0);
        this.accountID = UUID.randomUUID();
        this.transactions = new HashSet<>();
    }


    @Override
    public boolean equals(Object obj)
    {
        if(obj instanceof Account)
        {
            Account rhs = (Account) obj;
            return this.accountID.equals(rhs.getAccountID());
        }
        return false;
    }

    @Override
    public UUID getAccountID()
    {
        return this.accountID;
    }

    @Override
    public UUID getAccountHolderID()
    {
        return  this.userID;
    }

    @Override
    public BigDecimal getAccountBalance()
    {
        return this.balance;
    }

    @Override
    public void setAccountBalance(BigDecimal balance)
    {
        this.balance = balance;
    }

    @Override
    public Set<UUID> transactions()
    {
        return this.transactions;
    }
}
